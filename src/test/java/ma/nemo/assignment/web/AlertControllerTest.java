package ma.nemo.assignment.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.domain.Supply;
import ma.nemo.assignment.dto.ThresholdDto;
import ma.nemo.assignment.repository.ProductRepository;
import ma.nemo.assignment.repository.SupplyRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest(controllers = AlertController.class)
@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(MockitoExtension.class)
class AlertControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private ProductRepository productRepository;
    @MockBean
    private SupplyRepository supplyRepository;

    @Test
    public void ValidExpiryAlerts_ReturnList() throws Exception {

        // Create some supplies close to expiration date
        Product prd1 = Product.builder().productCode("ABC").productName("abc").unitPrice(25.0).quantityInStock(200).productId(2L).build();
        Product prd2 = Product.builder().productCode("EFG").productName("efg").unitPrice(25.0).quantityInStock(200).productId(1L).build();

        Supply supply1 = new Supply(new Date());
        supply1.setProduct(prd1);
        Supply supply2 = new Supply(new Date());
        supply2.setProduct(prd2);

        when(supplyRepository.findAll()).thenReturn(Arrays.asList(supply1, supply2));

        // Create unique product ids for these supplies
        when(productRepository.findAllById(any(List.class))).thenReturn(Arrays.asList(prd1, prd2));

        // Perform the mockMvc request to the "/api/expiry-alerts" endpoint
        mockMvc.perform(MockMvcRequestBuilders.get("/api/expiry-alerts"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("[{\"productId\":2,\"productCode\":\"ABC\",\"productName\":\"abc\",\"description\":null,\"unitPrice\":25.0,\"quantityInStock\":200,\"threshold\":null,\"creationDate\":null,\"modificationDate\":null},{\"productId\":1,\"productCode\":\"EFG\",\"productName\":\"efg\",\"description\":null,\"unitPrice\":25.0,\"quantityInStock\":200,\"threshold\":null,\"creationDate\":null,\"modificationDate\":null}]")); // JSON representation of the products
    }

    @Test
    public void ValidThresholdAlerts_ReturnList() throws Exception {
        // Create some products with set thresholds and quantities below those thresholds
        Product product1 = Product.builder()
                .productCode("ABC")
                .productName("abc")
                .unitPrice(25.0)
                .quantityInStock(5) // Quantity below threshold
                .threshold(10) // Threshold set
                .productId(2L)
                .build();

        Product product2 = Product.builder()
                .productCode("EFG")
                .productName("efg")
                .unitPrice(25.0)
                .quantityInStock(15) // Quantity below threshold
                .threshold(20) // Threshold set
                .productId(1L)
                .build();

        when(productRepository.findAll()).thenReturn(Arrays.asList(product1, product2));

        // Perform the mockMvc request to the "/api/threshold-alerts" endpoint
        mockMvc.perform(MockMvcRequestBuilders.get("/api/threshold-alerts"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("[{\"productId\":2,\"productCode\":\"ABC\",\"productName\":\"abc\",\"description\":null,\"unitPrice\":25.0,\"quantityInStock\":5,\"threshold\":10,\"creationDate\":null,\"modificationDate\":null},{\"productId\":1,\"productCode\":\"EFG\",\"productName\":\"efg\",\"description\":null,\"unitPrice\":25.0,\"quantityInStock\":15,\"threshold\":20,\"creationDate\":null,\"modificationDate\":null}]"));
    }

    @Test
    public void ValidSetThreshold_ReturnOk() throws Exception {
        // Create a valid ThresholdDto
        ThresholdDto thresholdDto = ThresholdDto.builder().productCode("prod1").quantity(10).build();

        // Mock the behavior of productRepository to return a product with the given product code
        Product product = Product.builder()
                .productCode("prod1")
                .productName("abc")
                .unitPrice(25.0)
                .quantityInStock(5) // Quantity below threshold
                .threshold(15) // Threshold set
                .productId(2L)
                .build();
        when(productRepository.findByProductCode(any(String.class))).thenReturn(product);

        // Perform the mockMvc request to the "/api/set-threshold" endpoint
        mockMvc.perform(MockMvcRequestBuilders.post("/api/set-threshold")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(thresholdDto)))
                .andExpect(MockMvcResultMatchers.status().isOk());

        // Ensure that the threshold was set correctly
        assertEquals(10, product.getThreshold());
    }

    @Test
    public void MissingProductCodeForSetThreshold_ReturnBadRequest() throws Exception {
        // Create a ThresholdDto without specifying the product code
        ThresholdDto thresholdDto = ThresholdDto.builder()
                .quantity(10)
                .build();

        // Perform the mockMvc request to the "/api/set-threshold" endpoint
        mockMvc.perform(MockMvcRequestBuilders.post("/api/set-threshold")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(thresholdDto)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void NullQuantityForSetThreshold_ReturnBadRequest() throws Exception {
        // Create a ThresholdDto with a null quantity
        ThresholdDto thresholdDto = ThresholdDto.builder()
                .productCode("prod1")
                .quantity(null)
                .build();

        // Perform the mockMvc request to the "/api/set-threshold" endpoint
        mockMvc.perform(MockMvcRequestBuilders.post("/api/set-threshold")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(thresholdDto)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


}