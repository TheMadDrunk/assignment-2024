package ma.nemo.assignment.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.dto.ProductDto;
import ma.nemo.assignment.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest(controllers = ProductController.class)
@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(MockitoExtension.class)
public class ProductControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ProductRepository productRepository;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void CreateProduct_ValidProduct_ReturnCreated() throws Exception {
        // Create a ProductDto
        ProductDto productDto = ProductDto.builder()
                .productCode("prod1")
                .productName("My Product")
                .unitPrice(25.0)
                .quantityInStock(100)
                .build();

        // Mock the behavior of productRepository
        when(productRepository.findByProductCode(any(String.class))).thenReturn(null);

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(productDto)))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void CreateProduct_ProductAlreadyExists_ReturnConflict() throws Exception {
        // Create a ProductDto
        ProductDto productDto = ProductDto.builder()
                .productCode("existcode")
                .productName("Existing Product")
                .unitPrice(30.0)
                .quantityInStock(50)
                .build();

        // Mock the behavior of productRepository to simulate an existing product
        Product existingProduct = Product.builder()
                .productCode("existcode")
                .productName("Existing Product")
                .unitPrice(30.0)
                .quantityInStock(50)
                .build();
        when(productRepository.findByProductCode(any(String.class))).thenReturn(existingProduct);

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(productDto)))
                .andExpect(MockMvcResultMatchers.status().isConflict());
    }

    @Test
    public void GetProduct_ValidProductId_ReturnProduct() throws Exception {
        // Mock the behavior of productRepository to return a product with a specific ID
        Long productId = 1L;
        Product product = Product.builder()
                .productId(productId)
                .productCode("prod1")
                .productName("My Product")
                .unitPrice(25.0)
                .quantityInStock(100)
                .build();
        when(productRepository.findById(productId)).thenReturn(Optional.of(product));

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.get("/api/products/{id}", productId))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void GetProduct_InvalidProductId_ReturnNotFound() throws Exception {
        // Mock the behavior of productRepository to simulate an invalid product ID
        Long invalidProductId = 999L;
        when(productRepository.findById(invalidProductId)).thenReturn(Optional.empty());

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.get("/api/products/{id}", invalidProductId))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

}