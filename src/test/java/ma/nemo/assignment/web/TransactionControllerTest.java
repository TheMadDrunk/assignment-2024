package ma.nemo.assignment.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.dto.ReturnDto;
import ma.nemo.assignment.dto.SaleDto;
import ma.nemo.assignment.dto.SupplyDto;
import ma.nemo.assignment.repository.ProductRepository;
import ma.nemo.assignment.repository.ReturnRepository;
import ma.nemo.assignment.repository.SaleRepository;
import ma.nemo.assignment.repository.SupplyRepository;
import ma.nemo.assignment.service.AuthService;
import ma.nemo.assignment.service.TransactionHistoryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest(controllers = TransactionController.class)
@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(MockitoExtension.class)
class TransactionControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ProductRepository productRepository;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private SupplyRepository supplyRepository;
    @MockBean
    private SaleRepository saleRepository;
    @MockBean
    private ReturnRepository returnRepository;
    @MockBean
    private AuthService authService;
    @MockBean
    private TransactionHistoryService transactionHistoryService;

    @Test
    public void SupplyTest_ValidSupply_ReturnCreated() throws Exception{
        // Create a supplyDto
        SupplyDto supplyDto = SupplyDto.builder().productCode("prod1").quantity(200).build();

        // Mock the behavior of productRepository
        Product product = Product.builder()
                .productName("myprod")
                .productCode("prod1")
                .unitPrice(25.0)
                .quantityInStock(100)
                .build();
        when(productRepository.findByProductCode(any(String.class))).thenReturn(product);

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/supply")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(supplyDto))) // Use the value returned by objectMapper
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void SupplyTest_InvalidProductCode_ReturnBadRequest() throws Exception {
        // Create a supplyDto with an invalid product code
        SupplyDto supplyDto = SupplyDto.builder().productCode("invalid_code").quantity(200).build();
        when(productRepository.findByProductCode(any(String.class))).thenReturn(null);
        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/supply")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(supplyDto)))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void SupplyTest_InvalidQuantity_ReturnBadRequest() throws Exception {
        // Create a supplyDto with an invalid quantity
        SupplyDto supplyDto = SupplyDto.builder().productCode("prod1").quantity(1000).build();

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/supply")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(supplyDto)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void SupplyTest_MissingProductCode_ReturnBadRequest() throws Exception {
        // Create a supplyDto without specifying the product code
        SupplyDto supplyDto = SupplyDto.builder().quantity(200).build();

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/supply")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(supplyDto)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void SupplyTest_NullQuantity_ReturnBadRequest() throws Exception {
        // Create a supplyDto with a null quantity
        SupplyDto supplyDto = SupplyDto.builder().productCode("prod1").quantity(null).build();

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/supply")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(supplyDto)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void SaleTest_ValidSale_ReturnCreated() throws Exception {
        // Create a valid SaleDto
        SaleDto saleDto = SaleDto.builder().productCode("prod1").quantity(5).build();

        // Mock the behavior of productRepository to return a product with sufficient quantity
        Product product = Product.builder()
                .productName("myprod")
                .productCode("prod1")
                .unitPrice(25.0)
                .quantityInStock(10) // Sufficient quantity
                .build();
        when(productRepository.findByProductCode(any(String.class))).thenReturn(product);

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/sale")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(saleDto)))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void SaleTest_ProductNotFound_ReturnNotFound() throws Exception {
        // Create a SaleDto with a non-existent product code
        SaleDto saleDto = SaleDto.builder().productCode("invalid_code").quantity(5).build();

        // Mock the behavior of productRepository to return null (product not found)
        when(productRepository.findByProductCode(any(String.class))).thenReturn(null);

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/sale")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(saleDto)))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void SaleTest_InsufficientQuantityInStock_ReturnBadRequest() throws Exception {
        // Create a SaleDto with a quantity that exceeds the available stock
        SaleDto saleDto = SaleDto.builder().productCode("prod1").quantity(15).build();

        // Mock the behavior of productRepository to return a product with insufficient quantity
        Product product = Product.builder()
                .productName("myprod")
                .productCode("prod1")
                .unitPrice(25.0)
                .quantityInStock(10) // Insufficient quantity
                .build();
        when(productRepository.findByProductCode(any(String.class))).thenReturn(product);

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/sale")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(saleDto)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void SaleTest_MissingProductCode_ReturnBadRequest() throws Exception {
        // Create a SaleDto without specifying the product code
        SaleDto saleDto = SaleDto.builder().quantity(5).build();

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/sale")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(saleDto)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void SaleTest_NullQuantity_ReturnBadRequest() throws Exception {
        // Create a SaleDto with a null quantity
        SaleDto saleDto = SaleDto.builder().productCode("prod1").quantity(null).build();

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/sale")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(saleDto)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void ReturnTest_ValidReturnProduct_ReturnCreated() throws Exception {
        // Create a valid ReturnDto
        ReturnDto returnDto = ReturnDto.builder()
                .productCode("prod1")
                .quantity(5)
                .reason("Defective product")
                .build();

        // Mock the behavior of productRepository to return a product with the given product code
        Product product = Product.builder()
                .productName("myprod")
                .productCode("prod1")
                .unitPrice(25.0)
                .quantityInStock(10)
                .build();
        when(productRepository.findByProductCode(any(String.class))).thenReturn(product);

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/return")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(returnDto)))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void ReturnTest_ProductNotFound_ReturnNotFound() throws Exception {
        // Create a ReturnDto with a non-existent product code
        ReturnDto returnDto = ReturnDto.builder()
                .productCode("invalid_code")
                .quantity(5)
                .reason("Defective product")
                .build();

        // Mock the behavior of productRepository to return null (product not found)
        when(productRepository.findByProductCode(any(String.class))).thenReturn(null);

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/return")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(returnDto)))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void ReturnTest_MissingProductCode_ReturnBadRequest() throws Exception {
        // Create a ReturnDto without specifying the product code
        ReturnDto returnDto = ReturnDto.builder()
                .quantity(5)
                .reason("Defective product")
                .build();

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/return")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(returnDto)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void ReturnTest_NullQuantity_ReturnBadRequest() throws Exception {
        // Create a ReturnDto with a null quantity
        ReturnDto returnDto = ReturnDto.builder()
                .productCode("prod1")
                .quantity(null)
                .reason("Defective product")
                .build();

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/return")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(returnDto)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void ReturnTest_ShortReason_ReturnBadRequest() throws Exception {
        // Create a ReturnDto with a short reason
        ReturnDto returnDto = ReturnDto.builder()
                .productCode("prod1")
                .quantity(5)
                .reason("a") // Short reason
                .build();

        // Perform the mockMvc request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/return")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(returnDto)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

}