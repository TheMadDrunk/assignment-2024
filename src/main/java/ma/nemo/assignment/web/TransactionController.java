package ma.nemo.assignment.web;

import jakarta.validation.Valid;
import ma.nemo.assignment.domain.*;
import ma.nemo.assignment.domain.util.TransactionType;
import ma.nemo.assignment.dto.ReturnDto;
import ma.nemo.assignment.dto.SaleDto;
import ma.nemo.assignment.dto.SupplyDto;
import ma.nemo.assignment.exceptions.InsufficientQuantityInStock;
import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.mapper.ReturnMapper;
import ma.nemo.assignment.mapper.SaleMapper;
import ma.nemo.assignment.mapper.SupplyMapper;
import ma.nemo.assignment.repository.ProductRepository;
import ma.nemo.assignment.repository.ReturnRepository;
import ma.nemo.assignment.repository.SaleRepository;
import ma.nemo.assignment.repository.SupplyRepository;
import ma.nemo.assignment.service.TransactionHistoryService;
import ma.nemo.assignment.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;



@RestController
public class TransactionController {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private SupplyRepository supplyRepository;
    @Autowired
    private SaleRepository saleRepository;
    @Autowired
    private ReturnRepository returnRepository;

    @Autowired
    private TransactionHistoryService transactionHistoryService;

    @Autowired
    private AuthService authService;
    @PostMapping("/api/supply")
    public ResponseEntity supply(@Valid @RequestBody SupplyDto supplydto) throws ProductNotFound {
        Product prd = productRepository.findByProductCode(supplydto.getProductCode());

        if(prd == null){
            throw new ProductNotFound();
        }

        Supply supply = SupplyMapper.mapToSupply(supplydto,prd);
        supply.setUser(authService.getAuth());
        supplyRepository.save(supply);

        prd.setQuantityInStock(prd.getQuantityInStock()+supply.getQuantity());
        productRepository.save(prd);

        transactionHistoryService.logInHistory(supply, TransactionType.SUPPLY);

        return new ResponseEntity<>(supply, HttpStatus.CREATED);
    }

    @GetMapping("/api/supply")
    public  ResponseEntity<List<Supply>> getSupplies(){
        List<Supply> supplies = supplyRepository.findAll();
        return new ResponseEntity<>(supplies,HttpStatus.OK);
    }

    @PostMapping("/api/sale")
    public  ResponseEntity sale(@Valid @RequestBody SaleDto saleDto) throws ProductNotFound, InsufficientQuantityInStock {
        Product prd = productRepository.findByProductCode(saleDto.getProductCode());

        if(prd == null){
            throw new ProductNotFound();
        } else if (prd.getQuantityInStock() < saleDto.getQuantity()) {
            throw new InsufficientQuantityInStock();
        }

        Sale sale = SaleMapper.mapToSale(saleDto,prd);
        sale.setUser(authService.getAuth());
        saleRepository.save(sale);

        transactionHistoryService.logInHistory(sale,TransactionType.SALE);

        prd.setQuantityInStock(prd.getQuantityInStock()-sale.getQuantity());
        productRepository.save(prd);

        return new ResponseEntity<>("Sale confirmed",HttpStatus.CREATED);
    }

    @GetMapping("/api/sale")
    public  ResponseEntity<List<Sale>> getSales(){
        List<Sale> sales = saleRepository.findAll();
        return new ResponseEntity<>(sales,HttpStatus.OK);
    }

    @PostMapping("/api/return")
    public ResponseEntity returnProduct(@Valid @RequestBody ReturnDto returnDto) throws ProductNotFound {
        Product prd = productRepository.findByProductCode(returnDto.getProductCode());

        if(prd == null){
            throw new ProductNotFound();
        }

        Return returnPrd = ReturnMapper.mapToReturn(returnDto,prd);
        returnPrd.setUser(authService.getAuth());
        returnRepository.save(returnPrd);

        transactionHistoryService.logInHistory(returnPrd, TransactionType.RETURN);

        prd.setQuantityInStock(prd.getQuantityInStock()+returnPrd.getQuantity());
        productRepository.save(prd);

        return new ResponseEntity<>("Product returned",HttpStatus.CREATED);
    }

    @GetMapping("/api/return")
    public  ResponseEntity<List<Return>> getReturns(){
        List<Return> returns = returnRepository.findAll();
        return new ResponseEntity<>(returns,HttpStatus.OK);
    }

    @GetMapping("/api/history")
    public ResponseEntity<List<TransactionHistory>> getHistory(){
        List<TransactionHistory> thlist = transactionHistoryService.getHistory();
        return new ResponseEntity<>(thlist,HttpStatus.OK);
    }


}
