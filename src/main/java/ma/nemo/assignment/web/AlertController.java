package ma.nemo.assignment.web;

import jakarta.validation.Valid;
import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.domain.Supply;
import ma.nemo.assignment.dto.ThresholdDto;
import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.repository.ProductRepository;
import ma.nemo.assignment.repository.SupplyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class AlertController {
    @Autowired
    private SupplyRepository supplyRepository;
    @Autowired
    private ProductRepository productRepository;

    private final int DAYS_BEFORE_EXPIRY_ALERT = 5;

    private boolean isCloseToExpiry(Supply supply){
        long expiryDate = supply.getExpirationDate().getTime();
        long MILLIS_IN_DAY = 24 * 60 * 60 * 1000;
        long compareDate = new Date().getTime() + DAYS_BEFORE_EXPIRY_ALERT * MILLIS_IN_DAY;
        return expiryDate < compareDate;
    }

    @GetMapping("/api/expiry-alerts")
    public ResponseEntity<List<Product>> expiryList(){
        //get the supplies close to Expiration Date
        List<Supply> supplies = supplyRepository.findAll()
                .stream()
                .filter(supply -> supply.getExpirationDate() != null && supply.getExpirationDate().after(new Date()))
                .filter(this::isCloseToExpiry).toList();

        //get unique ids out of the supplies list
        List<Long> ids = supplies
                .stream()
                .map(supply -> supply.getProduct().getProductId() )
                .distinct()
                .collect(Collectors.toList());

        //find list of products close to expiration Date using the list of ids
        List<Product> products = productRepository.findAllById(ids);
        if(products.isEmpty())
            return new ResponseEntity<>(null, HttpStatus.OK);
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/api/threshold-alerts")
    public ResponseEntity<List<Product>> belowThresholdList(){
        List<Product> products = productRepository.findAll()
                .stream()
                .filter(product -> product.getThreshold() != null)
                .filter(product -> product.getThreshold() > product.getQuantityInStock())
                .collect(Collectors.toList());
        if(products.isEmpty())
            return new ResponseEntity<>(null, HttpStatus.OK);
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @PostMapping("api/set-threshold")
    public ResponseEntity setThreshold(@Valid @RequestBody ThresholdDto thresholdDto) throws ProductNotFound {
        Product prd = productRepository.findByProductCode(thresholdDto.getProductCode());

        if(prd == null){
            throw new ProductNotFound();
        }

        prd.setThreshold(thresholdDto.getQuantity());
        productRepository.save(prd);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
