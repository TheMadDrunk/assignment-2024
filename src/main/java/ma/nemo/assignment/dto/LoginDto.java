package ma.nemo.assignment.dto;

import lombok.Data;

@Data
public class LoginDto {
    private String username;
    private String password;
}
