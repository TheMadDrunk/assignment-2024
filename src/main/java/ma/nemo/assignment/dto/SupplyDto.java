package ma.nemo.assignment.dto;


import jakarta.validation.constraints.*;
import lombok.Builder;
import lombok.Data;
import org.springframework.context.annotation.ComponentScan;

import java.util.Date;

@Data
@Builder
public class SupplyDto {

    @NotBlank(message = "productCode is required")
    private String productCode;

    @Min(value = 1, message = "quantity must be between 1 and 500")
    @Max(value = 500, message = "quantity must be between 1 and 500")
    @NotNull(message = "Quantity cannot be null")
    private Integer quantity;

    @Future(message = "Expiration date must be in the future")
    private Date expirationDate;
}
