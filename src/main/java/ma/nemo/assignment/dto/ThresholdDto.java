package ma.nemo.assignment.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ThresholdDto {
    @NotBlank(message = "productCode is required")
    private String productCode;

    @Min(value = 0, message = "quantity must be greater or equal to 0")
    @NotNull(message = "quantity cannot be null")
    private Integer quantity;
}
