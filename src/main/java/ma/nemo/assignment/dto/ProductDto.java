package ma.nemo.assignment.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class ProductDto {

  @NotBlank(message = "productCode is required")
  @Size(min = 3,max = 10,message = "productCode length should be between 3 and 10")
  private String productCode;

  @NotBlank(message = "productName is required")
  private String productName;

  private String description;

  @Min(value = 0, message = "unitPrice can't be negative")
  @NotNull(message = "quantity cannot be null")
  private Double unitPrice;

  @Min(value = 0, message = "quantity can't be negative")
  @NotNull(message = "quantity cannot be null")
  private Integer quantityInStock;

}
