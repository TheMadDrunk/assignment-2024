package ma.nemo.assignment.dto;

import jakarta.validation.constraints.*;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class SaleDto {
    @NotBlank(message = "productCode is required")
    private String productCode;

    @Min(value = 1, message = "quantity must be greater than 0")
    @NotNull(message = "quantity cannot be null")
    private Integer quantity;
}
