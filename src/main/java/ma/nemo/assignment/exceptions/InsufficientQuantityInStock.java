package ma.nemo.assignment.exceptions;

public class InsufficientQuantityInStock extends Exception{
    public InsufficientQuantityInStock() {
        super("Insufficient quantity in stock");
    }

    public InsufficientQuantityInStock(String message) {
        super(message);
    }
}
