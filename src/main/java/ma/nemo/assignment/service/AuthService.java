package ma.nemo.assignment.service;

import ma.nemo.assignment.domain.UserEntity;
import ma.nemo.assignment.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    @Autowired
    private UserRepository userRepository;

    public UserEntity getAuth(){
        Authentication auth = SecurityContextHolder. getContext(). getAuthentication();
        return userRepository.findByUsername(auth.getName()).get();
    }
}
