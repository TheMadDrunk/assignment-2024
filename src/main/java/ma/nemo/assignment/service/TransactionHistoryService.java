package ma.nemo.assignment.service;

import ma.nemo.assignment.domain.Transaction;
import ma.nemo.assignment.domain.TransactionHistory;
import ma.nemo.assignment.domain.util.TransactionType;
import ma.nemo.assignment.repository.TransactionHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionHistoryService {

    @Autowired
    private TransactionHistoryRepository tHRepository;
    public <T extends Transaction> void logInHistory(T transaction, TransactionType type){
        TransactionHistory th = new TransactionHistory();
        th.setProduct(transaction.getProduct());
        th.setUser(transaction.getUser());
        th.setQuantity(transaction.getQuantity());
        th.setTransactionDate(transaction.getTransactionDate());
        th.setTransactionType(type.getType());

        tHRepository.save(th);
    }

    public List<TransactionHistory> getHistory(){
        return tHRepository.findAll();
    }

}
