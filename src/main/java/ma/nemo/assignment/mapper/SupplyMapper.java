package ma.nemo.assignment.mapper;

import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.domain.Supply;
import ma.nemo.assignment.dto.SupplyDto;

import java.util.Date;

public class SupplyMapper {
    public static Supply mapToSupply(SupplyDto supplyDto, Product product) {
        Supply supply = new Supply();

        supply.setProduct(product);
        supply.setQuantity(supplyDto.getQuantity());
        supply.setExpirationDate(supplyDto.getExpirationDate());
        supply.setTransactionDate(new Date());

        return supply;
    }
}
