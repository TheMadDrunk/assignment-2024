package ma.nemo.assignment.mapper;

import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.domain.Return;
import ma.nemo.assignment.dto.ReturnDto;

import java.util.Date;

public class ReturnMapper {
    public static Return mapToReturn(ReturnDto returnDto, Product product) {
        Return returnPrd = new Return();

        returnPrd.setProduct(product);
        returnPrd.setQuantity(returnDto.getQuantity());
        returnPrd.setReason(returnDto.getReason());
        returnPrd.setTransactionDate(new Date());

        return returnPrd;
    }
}
