package ma.nemo.assignment.mapper;

import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.dto.ProductDto;

public class ProductMapper {
    public static Product mapToProduct(ProductDto dto){
        Product prd = new Product();

        prd.setProductCode(dto.getProductCode());
        prd.setProductName(dto.getProductName());
        prd.setDescription(dto.getDescription());
        prd.setUnitPrice(dto.getUnitPrice());
        prd.setQuantityInStock(dto.getQuantityInStock());

        return prd;
    }
}

