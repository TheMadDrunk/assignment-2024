package ma.nemo.assignment.mapper;

import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.domain.Sale;
import ma.nemo.assignment.dto.SaleDto;

import java.util.Date;

public class SaleMapper {
    public static Sale mapToSale(SaleDto saleDto, Product product) {
        Sale sale = new Sale();

        sale.setProduct(product);
        sale.setQuantity(saleDto.getQuantity());
        sale.setTotalPrice(product.getUnitPrice() * saleDto.getQuantity());
        sale.setTransactionDate(new Date());

        return sale;
    }
}
