package ma.nemo.assignment.domain.util;

public enum TransactionType {

  RETURN("return"),
  SUPPLY("supply"),
  SALE("sale");

  private String type;

  TransactionType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}
