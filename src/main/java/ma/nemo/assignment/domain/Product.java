package ma.nemo.assignment.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "Products")
public class Product {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long productId;

  @Column(unique = true, nullable = false)
  private String productCode;

  @Column(nullable = false)
  private String productName;

  private String description;

  private Double unitPrice;

  private Integer quantityInStock;

  private Integer threshold;

  /*@OneToMany(cascade = CascadeType.REMOVE)
  private List<Supply> supplies = new ArrayList<>();

  @OneToMany(mappedBy = "product", cascade = CascadeType.REMOVE)
  private List<Sale> sales = new ArrayList<>();

  @OneToMany(mappedBy = "product", cascade = CascadeType.REMOVE)
  private List<Return> returns = new ArrayList<>();*/

  @Temporal(TemporalType.TIMESTAMP)
  private Date creationDate;

  @Temporal(TemporalType.TIMESTAMP)
  private Date modificationDate;

  @PreUpdate
  public void preUpdate(){
    modificationDate = new Date();
  }

}